import xlsxwriter
from numpy import loadtxt
import numpy as np

risk = np.loadtxt('Risk.txt', delimiter=',', skiprows=0, usecols=(1,2), unpack=False)

Matrix = [[0 for x in range(5)] for x in range(5)]

for i in range(len(risk)):
    if Matrix[int(risk[i][0]-1)][int(risk[i][1]-1)] == 0:
        Matrix[int(risk[i][0]-1)][int(risk[i][1]-1)] = str(i+1)

    else:
        Matrix[int(risk[i][0]-1)][int(risk[i][1]-1)] = str(Matrix[int(risk[i][0]-1)][int(risk[i][1]-1)]) + ',' + str(i+1)
    

workbook = xlsxwriter.Workbook('Risk.xlsx')
worksheet = workbook.add_worksheet()

format2 = workbook.add_format()
format2.set_bg_color('silver')

worksheet.write(1,1,'Catastropic',format2)
worksheet.write(2,1,'Critical',format2)
worksheet.write(3,1,'Severe',format2)
worksheet.write(4,1,'Marginal',format2)
worksheet.write(5,1,'Negligible',format2)

worksheet.write(6,2,'Very unlikely',format2)
worksheet.write(6,3,'Unlikely',format2)
worksheet.write(6,4,'Moderate',format2)
worksheet.write(6,5,'Likely',format2)
worksheet.write(6,6,'Very likely',format2)

for i in range(5):
    for j in range(5):
        if not Matrix[i][j] == 0:
            worksheet.write(4-i+1,j+2,Matrix[i][j])


workbook.close()
