import numpy as np
import matplotlib.pyplot as plt

def calcPowerCruise(config,mass,n,rho,D,S,cd,V,LD):

    if config.lower()[:4] == "mult":
        forwardThrust = cd*0.5*rho*S*V*V
        lift = mass*9.81/n
        Phover = (lift**3.*2/(D*D*rho*3.1415))**0.5*n
        Pforward = forwardThrust*V

        Ptotal = Phover + Pforward

        return Ptotal

    if config.lower()[:4] == "sing" or config.lower()[:4] == "heli":
        forwardThrust = cd*0.5*rho*S*V*V
        lift = mass*9.81/n
        Phover = (lift**3.*2/(D*D*rho*3.1415))**0.5*n
        Pforward = forwardThrust*V

        Ptotal = Phover + Pforward

        return Ptotal
    
    if config.lower()[:4] == "hybr" or config.lower()[:4] == "conv":
        Pforward = mass*9.81/LD*V

        return Pforward

a = 20
b = 60

## single rotor
mass = 15.0
n = 1.0
rho = 0.73
D = 1.4
S = 0.4
cd = 0.3
LD = 0.0
energy = 15044689.2341
enduranceListSingle = []
coverageSingle = []

for V in range(a,b):

    P = calcPowerCruise('single',mass,n,rho,D,S,cd,float(V),LD)
    enduranceListSingle.append(energy/P/3600.)
    coverageSingle.append(energy/P/3600.*V)
    

## multi rotor
mass = 15.0
n = 4.0
rho = 0.73
D = 0.5
S = 0.4
cd = 0.3
LD = 0.0
energy = 19870344.2714
enduranceListMulti = []
coverageMulti = []
for V in range(a,b):

    P = calcPowerCruise('multi',mass,n,rho,D,S,cd,float(V),LD)
    enduranceListMulti.append(energy/P/3600.)
    coverageMulti.append(energy/P/3600.*V)

## hybrid
mass = 15.0
n = 4.0
rho = 0.73
D = 0.53
S = 0.6
cd = 0.3
LD = 10.0
energy = 8066565.06785*0.8
enduranceListHybrid = []
coverageHybrid = []
for V in range(a,b):

    P = calcPowerCruise('hybrid',mass,n,rho,D,S,cd,float(V),LD)
    enduranceListHybrid.append(energy/P/3600.)
    coverageHybrid.append(energy/P/3600.*V)

## conventional
mass = 15.0
n = 1.0
rho = 0.73
D = 0.1
S = 0.6
cd = 0.2
LD = 20.0
energy = 2065986.0
enduranceListConv = []
coverageConv = []

for V in range(a,b):

    P = calcPowerCruise('conventional',mass,n,rho,D,S,cd,float(V),LD)
    enduranceListConv.append(energy/P/3600.)
    coverageConv.append(energy/P/3600.*V)
    
Vlist = range(a,b)
plt.plot(Vlist,enduranceListSingle,label = 'Single Rotor')
plt.plot(Vlist,enduranceListMulti,label = 'Multi Rotor')
plt.plot(Vlist,enduranceListHybrid,label = 'Hybrid')
plt.plot(Vlist,enduranceListConv,label = 'Conventional')
plt.xlabel('Cruise Velocity [m/s]')
plt.ylabel('Endurance [hours]')
plt.legend()
plt.show()

plt.plot(Vlist,coverageSingle,label = 'Single Rotor')
plt.plot(Vlist,coverageMulti,label = 'Multi Rotor')
plt.plot(Vlist,coverageHybrid,label = 'Hybrid')
plt.plot(Vlist,coverageConv,label = 'Conventional')
plt.xlabel('Cruise Velocity [m/s]')
plt.ylabel('Coverage parameter [m]')
plt.legend()
plt.show()






    

