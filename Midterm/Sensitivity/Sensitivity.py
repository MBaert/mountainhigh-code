##from tempRick import *
##from tempMatthias import *
from sensDefinitions import *

lol = "rick is een aap"

increaseFactor = 1.05
decreaseFactor = 0.95

base_args_text = ['$\\rho$ [kg/$m^{3}$]','n [-]','D [m]','mass [kg]','safety factor H [-]','safety factor C [-]','energy [J]','endurance [s]','LD [-]','flight speed [m/s]','cruisefactor [-]','spring stiffness [N/m]','catapult length [m]','parachute mass [kg]']

# Sensitivity SingleRotor:
config = 'Single Rotor'
rho = 0.8
n = 1.0
D = 1.4
mass = 15.0
energy = 15044689.2341
safetyFactorH = 1.5
safetyFactorC = 0
endurance = 3600.0*2.
power = 'fuel'
LD = 0.0
flightSpeed = 0.0
cruisefactor = 0.0
k = 0.0
CataL = 0.0
ParaM = 0.0
base_args = [rho,n,D,mass,safetyFactorH,safetyFactorC,energy,endurance,LD,flightSpeed,cruisefactor,k,CataL,ParaM]

sensitivityAnalysis(base_args,base_args_text,config,power,increaseFactor,decreaseFactor)
    
# Sensitivity MultiRotor 1:
config = 'Multi Rotor'
rho = 0.8
n = 4.0
D = 0.53
mass = 15.0
energy = 19870344.2714
safetyFactorH = 1.5
safetyFactorC = 0
endurance = 3600.0*2.
power = 'Hybrid'
LD = 0.0
flightSpeed = 0.0
cruisefactor = 0.0
k = 0.0
CataL = 0.0
ParaM = 0.0
base_args = [rho,n,D,mass,safetyFactorH,safetyFactorC,energy,endurance,LD,flightSpeed,cruisefactor,k,CataL,ParaM]

sensitivityAnalysis(base_args,base_args_text,config,power,increaseFactor,decreaseFactor)


# Sensitivity MultiRotor 2:
config = 'Multi Rotor'
rho = 0.8
n = 4.0
D = 0.3
mass = 6.0
energy = 8776068.71987
safetyFactorH = 1.5
safetyFactorC = 0.
endurance = 3600.0/2.
power = 'Electric'
LD = 0.0
flightSpeed = 0.0
cruisefactor = 0.0
k = 0.0
CataL = 0.0
ParaM = 0.0
base_args = [rho,n,D,mass,safetyFactorH,safetyFactorC,energy,endurance,LD,flightSpeed,cruisefactor,k,CataL,ParaM]

sensitivityAnalysis(base_args,base_args_text,config,power,increaseFactor,decreaseFactor)



# Sensitivity Hybrid:
config = 'Hybrid'
rho = 0.8
n = 4.0
D = 0.53
mass = 15.0
energy = 8066565.06785
safetyFactorH = 1.5
safetyFactorC = 1.3
endurance = 3600.0*2.
power = 'hybrid'
LD = 10.
flightSpeed = 30.
cruisefactor = 0.75
k = 0.0
CataL = 0.0
ParaM = 0.0
base_args = [rho,n,D,mass,safetyFactorH,safetyFactorC,energy,endurance,LD,flightSpeed,cruisefactor,k,CataL,ParaM]

sensitivityAnalysis(base_args,base_args_text,config,power,increaseFactor,decreaseFactor)



# Sensitivity Conventional:
config = 'Conventional'
rho = 0.8
n = 2.0
D = 0.1
mass = 15.0
energy = 2065986.0
safetyFactorH = 1.5
safetyFactorC = 1.3
endurance = 3600.0*2.
power = 'electric'
LD = 20.
flightSpeed = 30.
cruisefactor = 1.
k = 4500.
CataL = 2.
ParaM = 2.
base_args = [rho,n,D,mass,safetyFactorH,safetyFactorC,energy,endurance,LD,flightSpeed,cruisefactor,k,CataL,ParaM]

sensitivityAnalysis(base_args,base_args_text,config,power,increaseFactor,decreaseFactor)





