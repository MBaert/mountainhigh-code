from mat2Lat import *
import copy
def sensitivityAnalysis(base_args,base_args_text,config,power,increaseFactor,decreaseFactor):


    """ Analyses the sensitivity of a certain configuration.
    Inputs: base_args      -> (list) with all parameters
            base_args_text -> (list) with all parameter names
            config         -> (string) configuration name
            power          -> (string) configuration name
            increaseFactor -> (float) factor used to increase the parameter
            decreaseFactor -> (float) factor used to decrease the parameter
            
    Output: Latex code for table with all the results
    
    Editor - Matthias
    """
    
    results = [['endurance (+) ','endurance (-) ','propulsion mass (+) ','propulsion mass (-) ']]

    ppmass = calc_ppmass(config,power,base_args)
    endurance = calc_endurance(config,power,base_args)
    
    for i in range(len(base_args)):
        args = copy.copy(base_args)
        if i == 1:
            args[i] = args[i] + 1
        else:
            args[i] = args[i]*increaseFactor
            
        enduranceNew1 = calc_endurance(config,power,args)
        ppmassNew1 = calc_ppmass(config,power,args)
        
        enduranceDiff1 = (enduranceNew1 - endurance)/endurance *100.
        ppmassDiff1 = (ppmassNew1 - ppmass)/ppmass *100.
        
        args = copy.copy(base_args)

        if i == 1:
            args[i] = max(args[i] - 1,1)
        else:
            args[i] = args[i]*decreaseFactor
            
        enduranceNew2 = calc_endurance(config,power,args)
        ppmassNew2 = calc_ppmass(config,power,args)
        
        enduranceDiff2 = (enduranceNew2 - endurance)/endurance *100.
        ppmassDiff2 = (ppmassNew2 - ppmass)/ppmass *100.

        if max(enduranceDiff1,enduranceDiff2,ppmassDiff1,ppmassDiff2) > 0.1 or min(enduranceDiff1,enduranceDiff2,ppmassDiff1,ppmassDiff2) < -0.1:
        
            results.append([base_args_text[i],enduranceDiff1,enduranceDiff2,ppmassDiff1,ppmassDiff2])
        
    matrix2Latex(results,caption="Sensitivity Analysis " + config,label="Sensitivity Analysis " + config)





def calc_endurance(config,power,args):

    """ Calculate the endurance of a certain configuration.
    Inputs: config         -> (string) configuration name
            power          -> (string) configuration name
            args           -> (list) values of different parameters
            
    Output: endurance
    
    Editor - Matthias
    """

    
    rho,n,D,mass,safetyFactorH,safetyFactorC,energy,endurance,LD,flightSpeed,cruisefactor,k,CataL,ParaM = args
    
    if config.lower()[:4] == 'mult':

        if n > 9:
            print 'Dat zijn veel rotors'

        T = float(mass)/n*9.81*safetyFactorH
        P = (T**3.*2/(D*D*rho*3.1415))**0.5*n
        endurance = energy/P
        
        return endurance


    elif config.lower()[:4] == 'sing' or config.lower()[:4] == 'heli':
        
        #rho,n,D,mass,energy,safetyFactor = args

        T = float(mass)/n*9.81*safetyFactorH
        P = (T**3.*2/(D*D*rho*3.1415))**0.5*n
        endurance = energy/P
        
        return endurance
        

    elif config.lower()[:4] == 'hybr':

        #rho,n,D,mass,energy,safetyFactorC,safetyFactorH,LD,flightSpeed,cruisefactor = args
        
        hoverfactor = 1.- cruisefactor
        
        Tcruise = float(mass)/n/LD*9.81*safetyFactorC
        Pcruise = Tcruise*flightSpeed*n

        Thover  = float(mass)/n*9.81*safetyFactorH
        Phover  = (Thover**3.*2/(D*D*rho*3.1415))**0.5*n

        P = cruisefactor * Pcruise + hoverfactor * Phover
        endurance = energy/P

        return endurance

    elif config.lower()[:4] == 'conv':
        
        #n,mass,energy,safetyFactor,LD,flightSpeed = args

        T = float(mass)/n/LD*9.81*safetyFactorC
        P = T*flightSpeed*n
        
        springenergy = 0.5*k*CataL*CataL
        
        endurance = (energy+springenergy)/P
        
        return endurance        

    else:
        print "No, no rockets - Edna"
        
def calc_ppmass(config,power,args):

    """ Calculate the power and propulsion mass of a certain configuration.
    Inputs: config         -> (string) configuration name
            power          -> (string) configuration name
            args           -> (list) values of different parameters
            
    Output: power and propulsion mass
    
    Editor - Rick
    """


    rho,n,D,mass,safetyFactorH,safetyFactorC,energy,endurance,LD,flightSpeed,cruisefactor,k,CataL,ParaM = args
    
    energyDensity1 = 46.6*10**6 #J/kg
    energyDensity2 = 0.9*10**6
        
    if config.lower()[:4] == "mult":
        
        #rho,n,D,mass,safetyFactor,endurance,power = args
        
        T = float(mass)/n*9.81*safetyFactorH
        P = (T**3.*2/(D*D*rho*3.1415))**0.5*n

        if power.lower()[:4] == "elec":
            ppmass = P*endurance/(energyDensity2) + (0.0003*P*2./n)*n  
                
        elif power.lower()[:4] == "hybr": 
            ppmass = P*endurance/(energyDensity1*0.4) + P*120/(energyDensity2) + (0.0003*P*2./n)*n + 0.0003*P + 0.6465 + 1.2

        else:
            print "Goodness, I know nothing about nuclear energy - James. D"

    elif config.lower()[:4] == "sing" or config.lower()[:3] == "heli":

        #rho,D,mass,safetyFactor,endurance = args
        #n = 1.
        
        T = float(mass)/n*9.81*safetyFactorH
        P = (T**3.*2/(D*D*rho*3.1415))**0.5*n

        ppmass = P*endurance/(energyDensity1*0.3) + 0.0003*P*2. + 0.6465 + 0.2      

    elif config.lower()[:4] == "hybr":

        #rho,n,D,mass,safetyFactorC,safetyFactorH,endurance,LD,flightSpeed,cruisefactor = args

        hoverfactor = 1.- cruisefactor

        Tcruise = float(mass)/n/LD*9.81*safetyFactorC
        Pcruise = Tcruise*flightSpeed*n

        Thover  = float(mass)/n*9.81*safetyFactorH
        Phover  = (Thover**3.*2/(D*D*rho*3.1415))**0.5*n

        P       = Phover * hoverfactor + Pcruise * cruisefactor
        #print P 
        ppmass  = P*endurance/(energyDensity1*0.5) + Phover*120/(energyDensity2) + (0.0003*Phover*2./n)*n + 0.0003*P + 0.6465 + 1.2
        
    elif config.lower()[:4] == "conv":

        #rho,n,D,mass,safetyFactor,endurance,LD,flightSpeed = args
        mass = mass + ParaM  #Weight for parachute
        T = float(mass)/n/LD*9.81*safetyFactorC
        P = T*flightSpeed*n

        ppmass = P*endurance/(energyDensity2) + (0.0003*P*2./n)*n

    else:
        print "No, no capes - Edna"

    return ppmass

