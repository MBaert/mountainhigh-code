
def matrix2Latex(datalist,caption="Caption",label="label"):
    """
    Turns a dataset into a Latex table.
    The data list should be of consistent size, only the first row may be smaller.
    
    Inputs:
    datalist    -> (list or array like) Set of data for in the table
    caption     -> (string) Tabel caption
    label       -> (string) Tabel label

    Outputs:
    latexTable  -> list or array like, filled with strings

    Editor - Rick
    """

    latexCodeTop = ["\\begin{table}[H]","\centering"]
    latexCodeBottom = ["\end{tabular}","\end{table}"]
    latexTable = [] + latexCodeTop

    # Format data list:
    if len(datalist[0]) + 1 == len(datalist[1]):
        datalist[0] = [" "] + datalist[0]
        
    for i in range(2,len(datalist)):
        if len(datalist[i]) !=  len(datalist[i-1]):
            print "Datalist is not of consistent dimensions. A table could not be constructed"


    # Create layout:
    latexTable.append("\\begin{tabular}{|l||" + (len(datalist[0])-1) * "c|" + "}" )
    latexTable.append("\hline")

    # Add lines:
    for row in datalist:
        latexRow = "" 
        for value in row:
            if type(value) == float:
                value = round(value,1)
            latexRow = latexRow + str(value)+ " & "
        latexRow = latexRow[:-2] + "\\\ \hline"
        if row == datalist[0]:
            latexRow = latexRow +" \hline"
        latexTable.append(latexRow)

    # Add bottom:
    latexTable.append(latexCodeBottom[0])
    latexTable.append("\caption{" + caption + "}")
    if label[:4] != "tab:":
        latexTable.append("\label{tab:" + label + "}")
    else: latexTable.append("\label{" + label + "}")
    latexTable.append(latexCodeBottom[1])

    for line in latexTable:
        print line
    print 
    return latexTable




    
    
