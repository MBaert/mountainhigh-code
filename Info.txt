- Zet je nieuwe code/mapje/afbeelding/whatever in de git map op je computer
- Open sourcetree 
- Eerst 'Pull' om nieuwe data binnen te halen (pijl naar beneden)
- Ga dan naar 'Commit' om je nieuwe data te uploaden. Geef ook een naam aan je commit bijv: 'Code van structural Main aangepast'
- Als laatste druk je op 'Push' om de nieuwe code echt te uploaden op de server

Moraal van het verhaal: Eerst pullen voor je pusht!

TIP: de eerste keer dat je wilt committen of pushen zal hij vragen om jezelf te identificeren. 
Ga rechtsboven naar settings en vul bij advanced je naam en email in